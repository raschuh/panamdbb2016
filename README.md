## Official Site of the 2016 Pan American Deaf Basketball Regional Qualification 

Formerly a private repo while the website was functional and now it is public as a part of my portfolio. 

The website was developed on behalf of a non-profit organization, 2016 Pan Am Deaf Basketball Calgary Organizing Committee, however the website never got fully implemented as the event has been relocated to Washington DC. 
The planned implementation included live streaming, online ticket ordering, volunteer management, and live scoring.  